/*	Definition section */
%{
    #include "common.h" //Extern variables that communicate with lex
    #define MAXSTACK 1000
    // #define YYDEBUG 1
    // int yydebug = 1;

    extern int yylineno;
    extern int yylex();
    extern FILE *yyin;
    extern char* yytext; 
    void yyerror (char const *s)
    {
        printf("error:%d: %s\n", yylineno, s);
    }

    /* Symbol table function - you can add new function if needed. */
    static void create_symbol();
    static void insert_symbol(char* a,int scope_lev);//insert symbol when declare
    static int lookup_symbol(char* a,int scope_lev);//use for checking redeclaration
    static int check_symbol(char* a,int scope_lev);//use for checkiong whether the particular symbol is defined or undefined
    static void dump_symbol();//print out the dump symbol table of the current ending block
    static int addr_symbol(char* a);//return the address of the given symbol
    static char* type_symbol(char* a);//return the type of the given symbol
    static char* ele_type_symbol(char* a);//return the element type of the given symbol
    static void push(char* s);//push the given operator into stack
    static char* pop();//pop an operator out from stack
    static void pop1(int a);//pop 'a' operators out from stack, error handing also under this function
    static void popAll();//pop all the operators out from stack, unused function
    static void checkTable();//print the symbol table, debug used
    static void pushv(char*s);//push the type of the given symbol into stackv
    static char* popv();//pop a element out from stackv

    /*global variable*/
    int ind=0;//used to set the row number of the symbol table
    int addr=0;//used to set the address of the symbol input
    int scope_lev=0;//used to indicate the current scope level
    symbol symTable[10000];//symbol table
    char* ytype;//indicate the element type
    int sign_size=0;//indicate number of 'POS' 'NEG' 'NOT' these three unary operators in the end of the stack
    int declare=0;//flag, is now declaration?1:0
    char* stack[MAXSTACK];//stack that save the operators
    int s_stack[MAXSTACK];//scope of operator,value of element indicates the number of operators in current scope
    int ss_size;//temp of element of s_stack
    int stack_size=0;//indicate the current size of stack now
    char* id_name;//temp to save id name
    char* expType;//indicate the expression type 
    int array=0;//flag, is now array?1:0
    char* id_type;//type of the id
    char* ele;//type of the element

    char* stackv[MAXSTACK];//stack that store the type of variables
    int stackv_size=0;//indicate the current size of stackv now
    int debug_mode=0;//flag, debug mode?1:0
%}

%error-verbose

/* Use variable or self-defined structure to represent
 * nonterminal and token type
 */
%union {
    int i_val;
    float f_val;
    char *s_val;
    struct lr_val{
        char* l_val;
        char* r_val;
    }
    /* ... */
}

/* Token without return */
%token VAR
%token INT FLOAT BOOL STRING
%token INC DEC
%token GEQ LEQ EQL NEQ
%token ADD_ASSIGN SUB_ASSIGN MUL_ASSIGN QUO_ASSIGN REM_ASSIGN
%token LAND LOR
%token NEWLINE
%token PRINT
%token PRINTLN
%token IF ELSE FOR
%token QUOTA

/* Token with return, which need to specify type */
%token <i_val> INT_LIT
%token <f_val> FLOAT_LIT
%token <s_val> IDENT
%token <s_val> STRING_LIT
%token <i_val> BOOL_LIT

/* Nonterminal with return, which need to specify type */
/*%type <type> Type TypeName ArrayType*/

/* Yacc will start at this nonterminal */
%start Program


/* Grammar section */
%%

Program
    : StatementList
;

StatementList
    : StatementList Statement
    | Statement
;

Type
    : TypeName
    | ArrayType
;

TypeName
    : INT 
        {ytype="int32";}
    | FLOAT 
        {ytype="float32";}    
    | STRING 
        {ytype="string";} 
    | BOOL 
        {ytype="bool";}
;

ArrayType
    : '['Expression']' Type 
        {
            pop1(s_stack[ss_size]);
            --ss_size;
            if(debug_mode==1) printf("\t[arr_declare]:--ss_size=%d\n",ss_size);
            array=1;
        }
;

Expression
    : UnaryExpr
    | Expression LOR_op Term1 
        {pop1(s_stack[ss_size]);expType="bool";}
    | Term1
        {pop1(s_stack[ss_size]);}
;

Term1
    : UnaryExpr
    | Term1 LAND_op Term2
        {expType="bool";}
    | Term2
;

Term2
    : UnaryExpr
    | Term2 cmp_op Term3
        {expType="bool";}
    | Term3
;   

Term3
    : UnaryExpr
    | Term3 add_op Term4
    | Term4
;

Term4
    : UnaryExpr
    | Term4 mul_op UnaryExpr {pop1(s_stack[ss_size]);}
;

UnaryExpr
    : PrimaryExpr
    | unary_op UnaryExpr
;


cmp_op 
    : EQL 
        {push("EQL");}
    | NEQ 
        {push("NEQ");}
    | '<' 
        {push("LSS");}
    | LEQ 
        {push("LEQ");}
    | '>' 
        {push("GTR");}
    | GEQ
        {push("GEQ");}
;

add_op
    : '+'
        {push("ADD");}
    |'-'
        {push("SUB");};

mul_op: '*' 
        {push("MUL");}
      |'/'
        {push("QUO");}
      |'%'
        {push("REM");}
;

unary_op 
    : '+' 
        {push("POS");sign_size+=1;}
    | '-' 
        {push("NEG");sign_size+=1;}
    | '!'
        {push("NOT");sign_size+=1;}
;

LAND_op: LAND {push("LAND");};
LOR_op:  LOR {push("LOR");};

PrimaryExpr
    : Operand
    | IndexExpr
    | ConversionExpr
;

Operand
    : Literal
    | identifier
    | '('Expression')'
        {
            pop1(s_stack[ss_size]);
            --ss_size;
            if(debug_mode==1) printf("\t(exp):--ss_size=%d\n",ss_size);
        }
;

Literal
    : INT_LIT
        {
            printf("INT_LIT %d\n",yylval.i_val);
            pushv("Lint32");
            pop1(sign_size);
            sign_size=0;
            expType="int32";
        }
    | FLOAT_LIT
        {
            printf("FLOAT_LIT %lf\n",yylval.f_val);
            pushv("Lfloat32");
            pop1(sign_size);
            sign_size=0;
            expType="float32";
        }
    | BOOL_LIT
        {
            printf("%s\n",yylval.i_val==1?"TRUE":"FALSE");
            pushv("Lbool"); 
            pop1(sign_size);
            sign_size=0;
            expType="bool";
        }
    | QUOTA STRING_LIT QUOTA
        {
            printf("STRING_LIT %s\n",yylval.s_val);
            pushv("Lstring"); 
            expType="string";
        }
;

IndexExpr
    : PrimaryExpr '['Expression']' 
        {
            popv();
            popv();
            pushv(ele);
            expType=ele;
            --ss_size;
            if(debug_mode==1) printf("\t[indExp]:--ss_size=%d\n",ss_size);
        }
;

ConversionExpr
    : Type '('Expression')' 
        {   
            printf("%c to %c\n",(expType[0]-'a'+'A'),(ytype[0]-'a'+'A'));
            popv();
            pushv(ytype);
            expType=ytype;
            --ss_size;
            if(debug_mode==1) printf("\t(conExp):--ss_size=%d\n",ss_size);
        }
;

Statement
    : DeclarationStmt NEWLINE
    | SimpleStmt NEWLINE
    | Block NEWLINE
    | IfStmt NEWLINE
    | ForStmt NEWLINE
    | PrintStmt NEWLINE
    | NEWLINE
;

SimpleStmt
    : AssignmentStmt
    | ExpressionStmt
    | IncDecStmt
;

DeclarationStmt 
    : Declaration 
        {insert_symbol(id_name,scope_lev);}
    | Declaration '=' Expression
        {
            insert_symbol(id_name,scope_lev);
            popv();
        }
;

Declaration
    : VAR identifier_dec Type
;

identifier
    : IDENT 
        {
            if(check_symbol(yylval.s_val,scope_lev)==-1){
                printf("error:%d: undefined: %s\n",yylineno+1,yylval.s_val);
            }else{
                printf("IDENT (name=%s, address=%d)\n",yylval.s_val,addr_symbol(yylval.s_val));
            }
            expType=id_type=type_symbol(yylval.s_val);
            ele=ele_type_symbol(yylval.s_val);
            pushv(expType);
        }
;

identifier_dec
    : IDENT {id_name=yylval.s_val;};

AssignmentStmt
   : Expression assign_op Expression {pop1(1);}
;

assign_op 
    : '='
        {push("ASSIGN");} 
    | ADD_ASSIGN 
        {push("ADD_ASSIGN");} 
    | SUB_ASSIGN 
        {push("SUB_ASSIGN");} 
    | MUL_ASSIGN 
        {push("MUL_ASSIGN");} 
    | QUO_ASSIGN 
        {push("QUO_ASSIGN");} 
    | REM_ASSIGN
        {push("REM_ASSIGN");} 
;

ExpressionStmt: Expression;

IncDecStmt : Expression INC
                {printf("INC\n");} 
           | Expression DEC
                {printf("DEC\n");}           
;

Block : '{'StatementList'}' {dump_symbol(scope_lev--);};

IfStmt
    : IF Condition Block 
    | IF Condition Block ELSE IfStmt
    | IF Condition Block ELSE Block
;

Condition
    : Expression
        {
            if(strcmp(expType,"bool")!=0)
                printf("error:%d: non-bool (type %s) used as for condition\n",yylineno+1,expType);
        }    
;

ForStmt
    : FOR Condition Block
    | FOR ForClause Block
;

ForClause: InitStmt ';' Condition ';' PostStmt;

InitStmt: SimpleStmt;
PostStmt: SimpleStmt;

PrintStmt
    : PRINT '(' Expression ')' 
        {
            printf("PRINT %s\n",expType);
            --ss_size;
            if(debug_mode==1) printf("\t(pf):--ss_size=%d\n",ss_size);
        }
    | PRINTLN '('Expression')' 
        {
            printf("PRINTLN %s\n",expType);
            --ss_size;
            if(debug_mode==1) printf("\t(pfln):--ss_size=%d\n",ss_size);
        }
;
%%

/* C code section */
int main(int argc, char *argv[])
{
    if (argc == 2) {
        yyin = fopen(argv[1], "r");
    } else {
        yyin = stdin;
    }

    yylineno = 0;
    yyparse();
    dump_symbol(0);
	printf("Total lines: %d\n", yylineno);
    fclose(yyin);
    return 0;
}

static void create_symbol() {
}

static void insert_symbol(char* a,int scope_lev) {
    int temp=lookup_symbol(a,scope_lev);
    if(temp!=-1){
        printf("error:%d: %s redeclared in this block. previous declaration at line %d\n",yylineno,a,temp);
        return;
    }
    printf("> Insert {%s} into symbol table (scope level: %d)\n",a, scope_lev);
    symTable[ind].name=a;
    symTable[ind].type=(array==1?"array":ytype);
    symTable[ind].addr=addr++;
    symTable[ind].lineno=yylineno;
    symTable[ind].ele_type=(array==1?ytype:"-");
    symTable[ind++].scope=scope_lev;
    //printf("%d %s %s %d %d %s %d\n",ind-1,symTable[ind-1].name,symTable[ind-1].type, symTable[ind-1].addr, symTable[ind-1].lineno,symTable[ind-1].ele_type,symTable[ind-1].scope);
    array=0;
}

static int lookup_symbol(char* a,int scope_lev) {
    for(int i=0;i<ind;++i){
        if(strcmp(symTable[i].name,a)==0 && symTable[i].scope==scope_lev)
            return symTable[i].lineno;     
    }
    return -1;
}

static int check_symbol(char* a,int scope_lev) {
    for(int i=0;i<ind;++i){
        if(strcmp(symTable[i].name,a)==0 && symTable[i].scope<=scope_lev)
            return symTable[i].lineno;     
    }
    return -1;
}

static void dump_symbol(int sl) {
    printf("> Dump symbol table (scope level: %d)\n", sl);
    printf("%-10s%-10s%-10s%-10s%-10s%s\n",
           "Index", "Name", "Type", "Address", "Lineno", "Element type");
    int j=0;
    for(int i=0;i<ind;++i)
      if(symTable[i].scope==sl) printf("%-10d%-10s%-10s%-10d%-10d%s\n",
            j++, symTable[i].name,symTable[i].type, symTable[i].addr, symTable[i].lineno,symTable[i].ele_type);
    ind-=j;
    if(debug_mode==1) printf("\t(dump_symbol):ind=%d\n",ind);
}

static int addr_symbol(char* a){
    int ans=-1;
    //checkTable(); 
    for(int i=0;i<ind;++i){
        if(strcmp(symTable[i].name,a)==0 && scope_lev>=symTable[i].scope)
            ans=symTable[i].addr;
    }
    return ans;    
}

static char* type_symbol(char* a){
    char* ans="undefined";
    for(int i=0;i<ind;++i){
        if(debug_mode==1) printf("\t(type_symbol):when i=%d\n",i);
        if(strcmp(symTable[i].name,a)==0 && scope_lev>=symTable[i].scope)
            ans=symTable[i].type;
    }
    return ans;    
}

static char* ele_type_symbol(char* a){
    char* ans="undefined";
    for(int i=0;i<ind;++i){
        if(strcmp(symTable[i].name,a)==0 && scope_lev>=symTable[i].scope)
            ans=symTable[i].ele_type;
    }
    return ans;    
}

static void push(char* s){
    if(debug_mode==1) printf("\t\t%s is pushed,ss_size=%d\n",s,ss_size);
    stack[stack_size++]=s;    
    ++s_stack[ss_size];
}

static char* pop(){
   return stack[--stack_size];    
}

static void pop1(int a){
   // printf("pop1(%d)\n",a);
    for(int i=0;i<a;++i){
       if(stack_size==0) break;
       char* op=pop();
       char* t1=popv();
       if(strcmp(op,"POS")==0 || strcmp(op,"NEG")==0 || strcmp(op,"NOT")==0){
        printf("%s\n",op);
        pushv(t1);
        --s_stack[ss_size];
       }else{
        if(t1[0]=='L') t1=t1+1;
        char* t2=popv();
        if(strcmp(t1,"undefined")==0 || strcmp(t2,"undefined")==0){
            printf("%s\n", op);
            --s_stack[ss_size];
        }else if(t2[0]=='L'&&(strcmp(op,"ASSIGN")==0 || strcmp(op,"ADD_ASSIGN")==0||strcmp(op,"SUB_ASSIGN")==0||strcmp(op,"MUL_ASSIGN")==0 || strcmp(op,"QUO_ASSIGN")==0 || strcmp(op,"REM_ASSIGN")==0)){
            printf("error:%d: cannot assign to %s\n",yylineno,t2+1);
            printf("%s\n", op);
            --s_stack[ss_size];
        }else{
            if(t2[0]=='L') t2=t2+1;
            if((strcmp(t1,"bool")!=0 && (strcmp(op,"LAND")==0 || strcmp(op,"LOR")==0)) || (strcmp(t1,"int32")!=0 && strcmp(op,"REM")==0)){
                printf("error:%d: invalid operation: (operator %s not defined on %s)\n",yylineno,op,t1);
            }else if((strcmp(t2,"bool")!=0 && (strcmp(op,"LAND")==0 || strcmp(op,"LOR")==0)) || (strcmp(t2,"int32")!=0 && strcmp(op,"REM")==0)){
                printf("error:%d: invalid operation: (operator %s not defined on %s)\n",yylineno,op,t2);
            }else if(strcmp(t1,t2)!=0){
                printf("error:%d: invalid operation: %s (mismatched types %s and %s)\n",yylineno,op,t2,t1);
            }else{
                if(strcmp(op,"EQL")==0 || strcmp(op,"NEQ")==0 || strcmp(op,"LSS")==0 || strcmp(op,"LEQ")==0 || strcmp(op,"GTR")==0 || strcmp(op,"GEQ")==0){
                    pushv("bool");
                }else{
                    pushv(t1);
                }
            }
            printf("%s\n", op);
            --s_stack[ss_size];
        }
       }
    }
}

static void popAll(){
   // printf("popAll()!\n");
    for(int i=stack_size-1;i>=0;--i)
        printf("%s\n",stack[i]);
    stack_size=0;    
}

static void checkTable(){
    for(int i=0;i<addr;++i) 
      printf("%-10d%-10s%-10s%-10d%-10d%s%-10d\n",i,symTable[i].name,symTable[i].type, symTable[i].addr, symTable[i].lineno,symTable[i].ele_type,symTable[i].scope);
           
}

static void pushv(char* s){
    stackv[stackv_size++]=s;    
    if(debug_mode==1) printf("\t(pushv):stackv_size++=%d,%s is pushed\n",stackv_size,s);
}

static char* popv(){
    if(debug_mode==1) printf("\t(popv):--stackv_size=%d,%s is pop!\n",stackv_size-1,stackv[stackv_size-1]);
    return stackv[--stackv_size];    
}
